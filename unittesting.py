#GBT Archive Unit Testing
#Brian Kent, NRAO, 2012

import gbtdump
import createaattables
import random
import unittest
import pyfits


class TestSequenceFunctions(unittest.TestCase):
    """Unit tests for methods in gbtdump.py and createaattables.py
    """

    def setUp(self):
        """Test data
        """
        self.datevalue = '2011-01-23T00:00:00'
        self.receiver = 'Rcvr8_10'
        self.testFolderName = 'testFolder'
        self.seq = [1, 2, 2, 3, 4, 5, 6, 7, 6, 5, 8, 9]
        self.project_path = '/export/cvlustre/vulcan/GBTDATA_NEW/AGBT11B_010_07'
        self.go_path = '/export/cvlustre/vulcan/GBTDATA_NEW/AGBT11B_010_07/GO/2012_01_24_13:05:41.fits'
        self.doc = {'TIMESYS': 'UTC', 'LASTON': 1, 'EXTEND': True, 'SCAN': 1,
            'SIMPLE': True, 'OBJECT': '1845+0953', 'GBTMCVER': '11.3', 'config_swtype': 'none',
            'MSGFLAG': '', 'NAXIS': 0, 'DATE-OBS': '2012-01-24T13:05:41', 'catalog_vel': '55.0',
            'NCHAN': 0, 'SIMULATE': 0, 'catalog_source': 'smithA', 'RESTFRQ': 8665000000.0,
            'OBSTYPE': 'CONTINUUM', 'PROCSIZE': 4, 'FITSVER': '2.7', 'BLOCK': 'teHigh',
            'LASTOFF': 0, 'config_restfreq': '8665', 'DEC': 9.8956666666666671,
            'RESTFRQ1': 8665000000.0, 'config_deltafreq': '0', 'config_beam': 'B1',
            'INSTRUME': 'Turtle', 'RA': 281.40687500000001, 'COORDSYS': 'RADEC',
            'VELDEF': 'VRAD-TOP', 'VELOCITY': 0.0, 'config_receiver': 'Rcvr8_10',
            'ORIGIN': 'NRAO Green Bank', 'config_bandwidth': '320', 'SWSTATE': 'NONE',
            'config_swmode': 'tp', 'OBSERVER': 'Dana Balser', 'config_vdef': 'Radio',
            'config_vframe': 'topo', 'PROJID': 'AGBT11B_010_07', 'PROCTYPE': 'POINTING',
            'catalog_location': 'J2000 @ (19:51:08.00, -01:00:10.00)', 'config_swper': '0.1',
            'config_tint': '0.1', 'config_vlow': '0.0', 'config_obstype': 'Continuum',
            'EQUINOX': 2000.0, 'SWTCHSIG': 'TPWCAL', 'config_vhigh': '0.0', 'config_nwin': '1',
            'config_swfreq': '0,0', 'PROCSEQN': 1, 'SKYFREQ': 8665000000.0, 'config_backend': 'DCR',
            'REFBEAM': '-1', 'DATEBLD': '2011-05-22T05:35:07', 'TELESCOP': 'NRAO_GBT',
            'PROCNAME': 'Peak', 'BW': 0, 'config_pol': 'Circular', 'BITPIX': 8,
            'RECEIVER': 'Rcvr8_10', 'OBSID': 'Off', 'RADESYS': 'FK5',
            'config_noisecal': 'lo', 'BACKEND': 'DCR'}
        self.coordsradec = {'COORDSYS': 'RADEC', 'EQUINOX': 2000.0, 'DATE-OBS': '2012-01-24T13:05:41',
            'RADESYS': 'FK5', 'RA': 185.645, 'DEC': 12.234}
        self.coordsgalac = {'COORDSYS': 'GALACTIC', 'EQUINOX': 2000.0, 'DATE-OBS': '2012-01-24T13:05:41',
            'RADESYS': 'FK5', 'GLON': 160.983, 'GLAT': 35.0543}
        self.coordsazel = {'COORDSYS': 'AZEL', 'EQUINOX': 2000.0, 'DATE-OBS': '2012-01-24T13:05:41',
            'RADESYS': 'FK5', 'AZ': 100.983, 'EL': 46.0543}

    def test_value(self):
        """Check conversion computation
        """
        testVal = gbtdump.gd2mjd(self.datevalue)
        self.assertEqual(testVal, 55584.0)

    def test_receiver(self):
        """Check receiver identifiers
        """
        testCase = gbtdump.receiver2band(self.receiver)
        self.assertEqual(testCase, 'X')

    def test_folderSize(self):
        """Check determination of folder size
        """
        testCase = gbtdump.getFolderSize('testFolder')
        self.assertEqual(testCase, 4133)

    def test_choiceUniqify(self):
        """Check to see that no unique elements are lost
        """
        element = random.choice(self.seq)
        uniqSeq = gbtdump.uniqify(self.seq)
        self.assertTrue(element in uniqSeq)

    def test_choiceUniqifyOrdered(self):
        """Check to see that no unique elements are lost
        """
        element = random.choice(self.seq)
        uniqSeq = gbtdump.uniqifyOrdered(self.seq)
        self.assertTrue(element in uniqSeq)

    def test_build_doc(self):
        """Check to see that dictionary output is correct
        """
        scanlogbinext = pyfits.getdata(self.project_path +
            '/ScanLog.fits', 'ScanLog')
        doc = gbtdump.build_doc(self.go_path, self.project_path, scanlogbinext)
        dockeylist = doc.keys()
        dockeylist.sort()
        self.assertEqual(dockeylist, sorted(self.doc))

    def test_computeCoordsradec(self):
        """Check coordinate computation ra/dec from python dictionary
        """
        doc = gbtdump.computeCoords(self.coordsradec)
        self.assertTrue('RAJ2000' in doc)
        self.assertTrue('DECJ2000' in doc)

    def test_computeCoordsgalactic(self):
        """Check coordinate computation galactic from python dictionary
        """
        doc = gbtdump.computeCoords(self.coordsgalac)
        self.assertTrue('RAJ2000' in doc)
        self.assertTrue('DECJ2000' in doc)

    def test_computeCoordsazel(self):
        """Check coordinate computation az/el from python dictionary
        """
        doc = gbtdump.computeCoords(self.coordsazel)
        self.assertTrue('RAJ2000' in doc)
        self.assertTrue('DECJ2000' in doc)

    def test_buildAATTables(self):
        """Check assembly of class gbt from sample metadata.csv file
        """
        gbt = createaattables.gbtarch()
        gbt = createaattables.buildAATTables('metadata.csv', gbt)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSequenceFunctions)
    unittest.TextTestRunner(verbosity=2).run(suite)
