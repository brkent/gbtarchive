#!/usr/bin/env python
"""This command line Python script takes metadata output from gbtdump.py
   and generates tables for the NRAO Archive Access Tool.
   Brian Kent, NRAO, 2012

Example usage::

    % python createaattables.py
"""
import csv
import glob
from os.path import join, dirname, abspath
import sys
import pyfits
import time
import string
import math
import decimal
from pyslalib import slalib


class gbtarch:
    """Store lists of Archive Access Tool (AAT) dictionaries

    """

    def __init__(self):
        self.projectids = []
        self.sessionids = []
        self.fileids = []
        self.file_set_ids = []
        self.freqids = []
        self.dateids = []
        self.observerids = []
        self.observationids = []
        self.bandids = []
        self.archiveentries = []
        self.archfilelocation = []
        self.file_set_properties = []
        self.datadesc = []
        self.obssummary = []


def gd2mjd(date_gd):
    """Returns the modified Julian date mjd as a float

    Keyword arguments:
    date_gd -- Takes a string input from GBT FITS DATE-OBS keyword
    in the form 'yyyy-mm-ddThh:mm:ss'

    Returns:
    Floating point modified julian date
    """

    date = string.split(date_gd, "T")
    date = string.split(date[0], "-")
    yyyy = int(date[0])
    mm = int(date[1])
    dd = int(date[2])

    time = string.split(date_gd, "T")
    time = string.split(time[1], ":")
    hh = float(time[0])
    min = float(time[1])
    sec = float(time[2])

    UT = hh + min / 60 + sec / 3600

    total_seconds = hh * 3600 + min * 60 + sec
    fracday = total_seconds / 86400

    if (100 * yyyy + mm - 190002.5) > 0:
        sig = 1
    else:
        sig = -1

    JD = 367 * yyyy - int(7 * (yyyy + int((mm + 9) / 12)) / 4) \
            + int(275 * mm / 9) + dd \
            + 1721013.5 + UT / 24 - 0.5 * sig + 0.5
    mjd = JD - 2400000.5

    return mjd


def uniqify(seq):
    """Returns a list with unique elements
       Does not preserve order
       #From http://www.peterbe.com/plog/uniqifiers-benchmark

    Keyword arguments:
    seq -- python list

    Returns:
    python list with duplicate elements removed
    """
    set = {}
    map(set.__setitem__, seq, [])
    return set.keys()


def uniqifyOrdered(seq, idfun=None):
    """Returns a list with unique elements
       DOES preserve order
       #From http://www.peterbe.com/plog/uniqifiers-benchmark

    Keyword arguments:
    seq -- python list
    idfun --

    Returns:
    ORDERED python list with duplicate elements removed
    """
    if idfun is None:

        def idfun(x):
            return x

    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen:
            continue
        seen[marker] = 1
        result.append(item)
    return result


def buildAATTables(file, gbt):
    """Method that reads a CSV formatted file of GBT metadata

    Keyword arguments:
    file - string name of the file to process
    gbt - instance of class gbtarch
    """
    dicts = []
    #Read in the header fields
    fields = [k for k in open('gbtindex-keywords.txt').read().split('\n') if k]
    reader = csv.DictReader(open(file), fields,
        delimiter=',', quoting=csv.QUOTE_ALL)
    # Skip first line
    next(reader)
    for row in reader:
        dicts.append(row)

    for i in dicts:
        onedeg = math.pi / 180.0
        freqdoc = {}
        datedoc = {}
        obsdoc = {}
        archivedoc = {}
        archfilelocationdoc = {}
        file_set_propertiesdoc = {}
        observationdoc = {}
        datadescdoc = {}
        obssummarydoc = {}
        banddoc = {}
        projid = i['PROJID'].rsplit('_')
        gbt.projectids.append(string.join(projid[0:len(projid)-1], '_'))
        gbt.sessionids.append(i['PROJID'])
        gbt.fileids.append(string.join(projid[0:len(projid)-1], '_'))
        gbt.file_set_ids.append(i['PROJID'])

        freqdoc['PROJID'] = i['PROJID']
        freqdoc['FRQS'] = (i['RESTFRQ'])
        gbt.freqids.append(freqdoc)

        datedoc['PROJID'] = i['PROJID']
        datedoc['DATE-OBS'] = i['DATE-OBS']
        gbt.dateids.append(datedoc)

        obsdoc['PROJID'] = i['PROJID']
        obsdoc['OBSERVER'] = i['OBSERVER']
        gbt.observerids.append(obsdoc)

        banddoc['PROJID'] = i['PROJID']
        banddoc['BAND'] = i['BAND']
        gbt.bandids.append(banddoc)

        #ARCHIVE TABLE
        archivedoc['PROJECT_CODE'] = string.join(projid[0:len(projid)-1], '_')
        archivedoc['SEGMENT'] = projid[len(projid)-1]
        #print >> sys.stderr, i
        archivedoc['STARTTIME'] = gd2mjd(i['DATE-OBS'])
        archivedoc['TELESCOPE'] = 'GBT'
        archivedoc['ARCH_FORMAT'] = 'FITS'
        archivedoc['DATA_TYPE'] = 'raw'
        archivedoc['ARCH_FILE'] = i['FILENAME']
        archivedoc['FILE_SIZE'] = i['FILESIZE'] #Needs to be in kB
        archivedoc['RAW_PROJECT_CODE'] = i['PROJID']
        archivedoc['OBS_BANDS'] = i['BAND']
        archivedoc['PROCNAME'] = i['PROCNAME']
        archivedoc['PROCTYPE'] = i['PROCTYPE']

        gbt.archiveentries.append(archivedoc)

        #ARCHFILELOCATION TABLE
        archfilelocationdoc['SITE_CODE'] = 'CV'
        archfilelocationdoc['ROOT_DIRECTORY'] = '/export/cvlustre/vulcan/'
        archfilelocationdoc['ARCH_FILE'] = i['FILENAME']

        gbt.archfilelocation.append(archfilelocationdoc)

        #OBSERVATION TABLE
        observationdoc['PROJECT_CODE'] = \
            string.join(projid[0:len(projid)-1], '_')
        observationdoc['SEGMENT'] = projid[len(projid)-1]
        observationdoc['STARTTIME'] = gd2mjd(i['DATE-OBS'])
        observationdoc['OBS_TYPE'] = i['PROCNAME']
        observationdoc['SOURCE_ID'] = i['OBJECT']
        if (i['RAJ2000'] != ''):
            #units of radians
            observationdoc['RA2000'] = str(float(i['RAJ2000']) * onedeg)
        else:
            observationdoc['RA2000'] = ''   #units of radians
        if (i['DECJ2000'] != ''):
            #units of radians
            observationdoc['DEC2000'] = float(i['DECJ2000']) * onedeg
        else:
            observationdoc['DEC2000'] = ''
        observationdoc['FRAME'] = 'ICRF'
        observationdoc['RA_EPOCH'] = 2000.0
        observationdoc['DEC_EPOCH'] = 2000.0

        gbt.observationids.append(observationdoc)

        #DATADESC TABLE
        datadescdoc['PROJECT_CODE'] = string.join(projid[0:len(projid)-1], '_')
        datadescdoc['SEGMENT'] = projid[len(projid)-1]
        datadescdoc['RECEIVER'] = i['RECEIVER']
        datadescdoc['BACKEND'] = i['BACKEND']
        datadescdoc['IF_BAND'] = i['BAND']
        datadescdoc['IF_REF_FREQ'] = i['RESTFRQ'] #units of Hz
        datadescdoc['POL1'] = '1'
        datadescdoc['SUB_NUM_CHANS'] = i['NCHAN']
        datadescdoc['VELOCITY'] = i['VELOCITY']
        datadescdoc['VELDEF'] = i['VELDEF']
        datadescdoc['VERSION'] = 1.0
        datadescdoc['MSGFLAG'] = i['MSGFLAG']

        gbt.datadesc.append(datadescdoc)

        #OBSSUMARY TABLE
        obssummarydoc['PROJECT_CODE'] = \
            string.join(projid[0:len(projid)-1], '_')
        obssummarydoc['SOURCE_ID'] = i['OBJECT']
        obssummarydoc['IF_BAND'] = i['BAND']
        obssummarydoc['STARTTIME'] = gd2mjd(i['DATE-OBS'])
        obssummarydoc['TELESCOPE'] = 'GBT'
        obssummarydoc['SUB_NUM_CHANS'] = i['NCHAN']
        if (i['RAJ2000'] != ''):
            #units of radians
            obssummarydoc['RA2000'] = str(float(i['RAJ2000']) * onedeg)
        else:
            obssummarydoc['RA2000'] = ''   #units of radians
        if (i['DECJ2000'] != ''):
            #units of radians
            obssummarydoc['DEC2000'] = float(i['DECJ2000']) * onedeg
        else:
            obssummarydoc['DEC2000'] = ''
        obssummarydoc['RADEG'] = i['RAJ2000']  #in degrees
        obssummarydoc['DECDEG'] = i['DECJ2000'] #in degrees
        obssummarydoc['OBSAZ'] = i['OBSAZ']
        obssummarydoc['OBSEL'] = i['OBSEL']
        #obssummarydoc['PROCNAME'] = i['PROCNAME']
        #obssummarydoc['PROCTYPE'] = i['PROCTYPE']

        gbt.obssummary.append(obssummarydoc)

    gbt.projectids = uniqify(gbt.projectids)
    gbt.sessionids = uniqify(gbt.sessionids)

    return gbt


#-------------------------------------------------------------------
def printProjects(fileout, gbt):
    """Print out AAT Project Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """
    gbt.projectids.sort()
    gbt.sessionids.sort()
    sessionids2 = []

    #Determine session ids
    print >> sys.stderr, 'Determining session IDs to count projects'
    for i in gbt.sessionids:
        sessid = i.rsplit('_')
        sessionids2.append(string.join(sessid[0:len(sessid)-1], '_'))

    #Count number of sessions per project
    fieldnames = ('PROJECT_CODE', 'OBSERVER', 'STARTTIME', 'STOPTIME',
        'TELESCOPE', 'OBS_BANDS', 'NUM_SEGMENTS', 'ARCH_FILES')
    f = open(fileout, 'w')
    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)

    projdictlist = []

    for i in gbt.projectids:
        doc = {}
        freqs = []
        dates = []
        bands = []
        observers = []
        sessindices = [j for j, x in enumerate(sessionids2) if x == i]
        fileindices = [j for j, x in enumerate(gbt.fileids) if x == i]
        for k in fileindices:
            freqs.append(gbt.freqids[k]['FRQS'])
            bands.append(gbt.bandids[k]['BAND'])
            dates.append(gbt.dateids[k]['DATE-OBS'])
            observers.append(gbt.observerids[k]['OBSERVER'])

        freqs = [x for x in freqs if x] #remove blanks
        bands = [x for x in bands if x] #remove blanks
        bands = uniqifyOrdered(bands)

        if (freqs != [] and freqs[0] != ''):
            freqs = map(float, uniqifyOrdered(freqs))

        #Find starting and ends dates of project
        dates.sort()

        #find observers
        observers = uniqify(observers)

        #Create final dictionary
        doc['PROJECT_CODE'] = i
        doc['OBSERVER'] = str(', '.join(observers))
        doc['STARTTIME'] = str(gd2mjd(dates[0]))
        doc['STOPTIME'] = str(gd2mjd(dates[-1]))
        #doc['PROPRIETARY'] = gd2mjd(dates[-1])+365.25
        doc['TELESCOPE'] = 'GBT'
        doc['OBS_BANDS'] = str(', '.join(bands))
        doc['NUM_SEGMENTS'] = str(len(sessindices))
        doc['ARCH_FILES'] = str(len(fileindices))

        projdictlist.append(doc)

        writer.writerow(doc)

    f.close()


#---------------------------------------------
def printArchive(fileout, gbt):
    """Print out AAT Archive Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """

    fieldnames = ('PROJECT_CODE', 'SEGMENT', 'STARTTIME', 'TELESCOPE',
        'ARCH_FORMAT', 'DATA_TYPE', 'ARCH_FILE', 'FILE_SIZE',
        'RAW_PROJECT_CODE', 'OBS_BANDS', 'PROCNAME', 'PROCTYPE')

    #Open file for write
    f = open(fileout, 'w')
    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)
    archivelist = gbt.archiveentries

    for archivedoc in archivelist:
        writer.writerow(archivedoc)

    f.close()


#---------------------------------------------------
def printArchfilelocation(fileout, gbt):
    """Print out AAT Arch file location Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """
    fieldnames = ('SITE_CODE', 'ROOT_DIRECTORY', 'ARCH_FILE')

    f = open(fileout, 'w')

    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)
    archfilelist = gbt.archfilelocation

    for archfiledoc in archfilelist:
        writer.writerow(archfiledoc)

    f.close()


#-------------------------------------------
def printFile_Set_Properties(fileout, gbt):
    """Print out AAT File Set Properties Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """

    fieldnames = ('FILE_SET_ID', 'FILE_SET_CATEGORY', 'N_FILE_SUBSETS',
        'COLLECTION', 'CALIB_LEVEL', 'STARTTIME',
        'STOPTIME', 'TELESCOPE', 'OBS_BANDS')
    f = open(fileout, 'w')
    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)

    file_set_propertiesdoc = {}

    for i in gbt.sessionids:
        freqs = []
        bands = []
        dates = []
        file_set_propertiesdoc['FILE_SET_ID'] = i
        file_set_propertiesdoc['FILE_SET_CATEGORY'] = 'SESSION'
        #files in an observing session
        fileindices = [j for j, x in enumerate(gbt.file_set_ids) if x == i]
        file_set_propertiesdoc['N_FILE_SUBSETS'] = len(fileindices)

        for k in fileindices:
            freqs.append(gbt.freqids[k]['FRQS'])
            bands.append(gbt.bandids[k]['BAND'])
            dates.append(gbt.dateids[k]['DATE-OBS'])
        #find freq bands
        freqs = [x for x in freqs if x] #remove blanks
        bands = [x for x in bands if x] #remove blanks
        bands = uniqifyOrdered(bands)

        if (freqs != [] and freqs[0] != ''):
            freqs = map(float, uniqifyOrdered(freqs))

        #Find earliest observing date/time for session
        dates.sort()

        file_set_propertiesdoc['COLLECTION'] = 'GBT_ARCH'
        file_set_propertiesdoc['CALIB_LEVEL'] = '0'
        file_set_propertiesdoc['STARTTIME'] = gd2mjd(dates[0])
        file_set_propertiesdoc['STOPTIME'] = gd2mjd(dates[-1])
        file_set_propertiesdoc['TELESCOPE'] = 'GBT'
        file_set_propertiesdoc['OBS_BANDS'] = bands

        gbt.file_set_properties.append(file_set_propertiesdoc)

        writer.writerow(file_set_propertiesdoc)

    f.close()


#-------------------------------------------------------------
def printObservation(fileout, gbt):
    """Print out AAT Observation Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """

    fieldnames = ('PROJECT_CODE', 'SEGMENT', 'OBS_TYPE', 'STARTTIME',
        'SOURCE_ID', 'RA2000', 'DEC2000', 'FRAME', 'RA_EPOCH', 'DEC_EPOCH')

    f = open(fileout, 'w')
    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)
    observationlist = gbt.observationids

    for observationdoc in observationlist:
        writer.writerow(observationdoc)

    f.close()


#------------------------------------------------------------
def printDatadesc(fileout, gbt):
    """Print out AAT Data Desc Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """

    fieldnames = ('PROJECT_CODE', 'SEGMENT', 'RECEIVER', 'BACKEND',
        'IF_BAND', 'IF_REF_FREQ', 'POL1', 'SUB_NUM_CHANS', 'VELOCITY',
        'VELDEF', 'VERSION', 'MSGFLAG')

    f = open(fileout, 'w')
    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)

    datadesclist = gbt.datadesc

    for datadescdoc in datadesclist:
        writer.writerow(datadescdoc)

    f.close()


#------------------------------------------------------------
def printObssummary(fileout, gbt):
    """Print out AAT Obs Summary Table

    Keyword arguments:
    fileout -- string name of the file
    gbt -- instance of class gbtarch
    """

    fieldnames = ('PROJECT_CODE', 'SOURCE_ID', 'IF_BAND', 'STARTTIME',
        'TELESCOPE', 'SUB_NUM_CHANS', 'RA2000', 'DEC2000',
        'RADEG', 'DECDEG', 'OBSAZ', 'OBSEL')
    f = open(fileout, 'w')
    writer = csv.DictWriter(f, fieldnames, \
            delimiter=',', quoting=csv.QUOTE_ALL)

    headers = dict((n, n) for n in fieldnames)
    writer.writerow(headers)

    obssummarylist = gbt.obssummary

    for obssummarydoc in obssummarylist:
        writer.writerow(obssummarydoc)

    f.close()

#-----------------------------------main script starts here
files = []

if len(sys.argv) == 1:
    msg = ('Sample usage - createaattables.py ' +
            'gbtmetadata.csv')
    #sys.exit(msg)
    print >> sys.stderr, msg

if len(sys.argv) > 1:
    files = sys.argv[1:]
    if not os.path.exists('aatexport'):
        os.makedirs('aatexport')

files.sort()

#Create instance of class gbtarch
gbt = gbtarch()

for file in files:
    print >> sys.stderr, file
    gbt = buildAATTables(file, gbt)

printProjects('aatexport/projects.txt', gbt)
printArchive('aatexport/archive.txt', gbt)
printArchfilelocation('aatexport/archfilelocation.txt', gbt)
printFile_Set_Properties('aatexport/file_set_properties.txt', gbt)
printObservation('aatexport/observation.txt', gbt)
printDatadesc('aatexport/datadesc.txt', gbt)
printObssummary('aatexport/obssummary.txt', gbt)

#Count number of files per project

print >> sys.stderr, len(gbt.projectids), 'unique project IDs'
print >> sys.stderr, len(gbt.sessionids), 'unique session IDs'
print >> sys.stderr, len(gbt.fileids), 'unique files (scans)'
