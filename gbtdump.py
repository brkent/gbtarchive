#!/usr/bin/env python
"""This command line Python script mines GBT observing sessions and extracts
   all GO FITS primary, configuration, and catalog keywords listed
   in the text file gbtindex-keywords.txt.  The standard argument
   passed to functions is a GBT observing session directory.
   Brian Kent, NRAO, 2012

Example usage::

% python gbtdump.py
    /export/cvlustre/vulcan/GBTDATA_NEW/AGBT11B_010_07 > CSV/output.csv
"""

import sys
import os
from os.path import join
import pyfits
import csv
import glob
import time
import string
import math
from pyslalib import slalib


def getFolderSize(folder):
    """Return the size of a folder in bytes

    Keyword arguments:
    folder -- string name of the folder

    Returns:
    integer value in bytes of the folder
    """
    total_size = os.path.getsize(folder)
    for item in os.listdir(folder):
        itempath = os.path.join(folder, item)
        if os.path.isfile(itempath):
            total_size += os.path.getsize(itempath)
        elif os.path.isdir(itempath):
            total_size += getFolderSize(itempath)
    return total_size


def gd2mjd(date_gd):
    """Returns the modified Julian date mjd as a float

    Keyword arguments:
    date_gd -- Takes a string input from GBT FITS DATE-OBS keyword
    in the form 'yyyy-mm-ddThh:mm:ss'

    Returns:
    Floating point modified julian date
    """

    date = string.split(date_gd, "T")
    date = string.split(date[0], "-")
    yyyy = int(date[0])
    mm = int(date[1])
    dd = int(date[2])

    time = string.split(date_gd, "T")
    time = string.split(time[1], ":")
    hh = float(time[0])
    min = float(time[1])
    sec = float(time[2])

    UT = hh + min / 60 + sec / 3600

    total_seconds = hh * 3600 + min * 60 + sec
    fracday = total_seconds / 86400

    if (100 * yyyy + mm - 190002.5) > 0:
        sig = 1
    else:
        sig = -1

    JD = 367 * yyyy - int(7 * (yyyy + int((mm + 9) / 12)) / 4) \
            + int(275 * mm / 9) + dd \
            + 1721013.5 + UT / 24 - 0.5 * sig + 0.5
    mjd = JD - 2400000.5

    return mjd


def receiver2band(receiver):
    """Returns the band for a given receiver with a dictionary

    Keyword arguments:
    receiver -- string name of the receiver

    Returns:
    String name of the receiver band
    """

    recvBands = {'Rcvr_342': 'PF1',
                'Rcvr_450': 'PF1',
                'Rcvr_600': 'PF1',
                'Rcvr_800': 'PF1',
                'Rcvr_1070': 'PF2',
                'Rcvr1_2': 'L',
                'Rcvr2_3': 'S',
                'Rcvr4_6': 'C',
                'Rcvr8_10': 'X',
                'Rcvr12_18': 'Ku',
                'Rcvr18_22': 'K(lower)',
                'Rcvr22_26': 'K(upper)',
                'RcvrArray18_26': 'KFPA',
                'Rcvr_26_40': 'Ka',
                'Rcvr40_52': 'Q',
                'Rcvr_PAR': 'MUSTANG',
                'NoiseSource': 'NoiseSource'}

    if (receiver in recvBands):
        band = recvBands[receiver]
    else:
        band = 'Other'

    return band


def uniqify(seq):
    """Returns a list with unique elements
       Does not preserve order
       #From http://www.peterbe.com/plog/uniqifiers-benchmark

    Keyword arguments:
    seq -- python list

    Returns:
    python list with duplicate elements removed
    """
    set = {}
    map(set.__setitem__, seq, [])
    return set.keys()


def uniqifyOrdered(seq, idfun=None):
    """Returns a list with unique elements
       DOES preserve order
       #From http://www.peterbe.com/plog/uniqifiers-benchmark

    Keyword arguments:
    seq -- python list
    idfun --

    Returns:
    ORDERED python list with duplicate elements removed
    """
    if idfun is None:

        def idfun(x):
            return x

    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen:
            continue
        seen[marker] = 1
        result.append(item)
    return result


def computeCoords(doc):
    """Returns a list of python dictionaries
       Computes the RA and DEC J2000 coordinates from GBT FITS metadata

    Keyword arguments:
    doc - list of python dictionaries, each with GBT metadata

    Returns:
    list of python dictionaries with GBT metadata
    """
    onedeg = math.pi / 180.0
    doc['RAJ2000'] = ''
    doc['DECJ2000'] = ''
    doc['OBSAZ'] = ''
    doc['OBSEL'] = ''

    if ('RADEC' in doc['COORDSYS']):
        #For rare files that do not have equinoxes
        if not ('EQUINOX' in doc):
            doc['EQUINOX'] = 2000.0

        if((doc['EQUINOX'] == 2000.0) or ('FK5' in doc['RADESYS'])):
            doc['RAJ2000'] = doc['RA'] #result in degrees
            doc['DECJ2000'] = doc['DEC'] #result in degrees
        if((doc['EQUINOX'] == 1950.0) or ('FK4' in doc['RADESYS'])):
            r1950 = doc['RA'] * onedeg  #result in radians
            d1950 = doc['DEC'] * onedeg #result in radians
            r2000, d2000 = slalib.sla_fk45z(r1950, d1950, 1950.0)
            doc['RAJ2000'] = r2000 / onedeg #result in degrees
            doc['DECJ2000'] = d2000 / onedeg #result in degrees
        if ((doc['EQUINOX'] == 2000.0) and ('GAPPT' in doc['RADESYS'])):
            mjd = gd2mjd(doc['DATE-OBS'])
            ra = doc['RA'] * onedeg #result in radians
            dec = doc['DEC'] * onedeg #result in radians
            amprms = slalib.sla_mappa(2000.0, mjd)
            rm, dm = slalib.sla_ampqk(ra, dec, amprms)
            doc['RAJ2000'] = rm / onedeg #result in degrees
            doc['DECJ2000'] = dm / onedeg #result in degrees
    if ('GALACTIC' in doc['COORDSYS']):
        dl = doc['GLON'] * onedeg #result in radians
        db = doc['GLAT'] * onedeg #result in radians
        dr, dd = slalib.sla_galeq(dl, db)
        doc['RAJ2000'] = dr / onedeg
        doc['DECJ2000'] = dd / onedeg
    if ('AZEL' in doc['COORDSYS']):
        az = doc['AZ'] * onedeg #result in radians
        el = doc['EL'] * onedeg #result in radians
        c, name, w, phi, h = slalib.sla_obs(0, "GBT")
        ha, dec = slalib.sla_dh2e(az, el, phi)
        gmst = slalib.sla_gmst(gd2mjd(doc['DATE-OBS']))
        last = gmst - w + slalib.sla_eqeqx(gd2mjd(doc['DATE-OBS']))
        ra = last - ha #result in radians
        doc['RAJ2000'] = ra / onedeg
        doc['DECJ2000'] = dec / onedeg
    if ('ENCODERAZEL' in doc['COORDSYS']):
        az = doc['AZ'] * onedeg #result in radians
        el = doc['EL'] * onedeg #result in radians
        c, name, w, phi, h = slalib.sla_obs(0, "GBT")
        ha, dec = slalib.sla_dh2e(az, el, phi)
        gmst = slalib.sla_gmst(gd2mjd(doc['DATE-OBS']))
        last = gmst - w + slalib.sla_eqeqx(gd2mjd(doc['DATE-OBS']))
        ra = last - ha #result in radians
        doc['RAJ2000'] = ra / onedeg
        doc['DECJ2000'] = dec / onedeg
    if ('HADEC' in doc['COORDSYS']):
        ha = doc['HA'] * onedeg #radians need to check...
        dec = doc['DEC'] * onedeg
        c, name, w, phi, h = slalib.sla_obs(0, "GBT")
        gmst = slalib.sla_gmst(gd2mjd(doc['DATE-OBS']))
        last = gmst - w + slalib.sla_eqeqx(gd2mjd(doc['DATE-OBS']))
        ra = last - ha #result in radians
        doc['RAJ2000'] = ra / onedeg
        doc['DECJ2000'] = dec / onedeg

    #compute Azimuth and Elevation
    if (doc['RAJ2000'] != '' and doc['DECJ2000'] != ''):
        c, name, w, phi, h = slalib.sla_obs(0, "GBT")
        gmst = slalib.sla_gmst(gd2mjd(doc['DATE-OBS']))
        last = gmst - w + slalib.sla_eqeqx(gd2mjd(doc['DATE-OBS']))
        ha = last - doc['RAJ2000'] * onedeg #radians
        az, el = slalib.sla_de2h(ha, doc['DECJ2000'] * onedeg, phi)
        doc['OBSAZ'] = az / onedeg #units of degrees
        doc['OBSEL'] = el / onedeg #units of degrees

    return doc


def derivedMetadata(doc, filename, filesize):
    """Caculates derived GBT metadata using information from the GO FITS files
       Does not preserve order
       #From http://www.peterbe.com/plog/uniqifiers-benchmark

    Keyword arguments:
    doc -- list of python dictionaries with GBT metadata
    filename -- string name of GBT observing session directory
    filesize -- float size of GBT observing session directory

    Returns:
    list of python dictionaries with GBT metadata
    """

    if "COORDSYS" in doc:
        doc = computeCoords(doc)
    doc['VERSION'] = 3.0
    projid = doc['PROJID'].rsplit('_')
    doc['PROPID'] = string.join(projid[0:len(projid)-1], '-')
    doc['BAND'] = receiver2band(doc['RECEIVER'])
    doc['FILESIZE'] = filename
    doc['FILENAME'] = filesize

    return doc


def interrupt(message=''):
    """Exit if interrupted
    """
    print >> sys.stderr, 'Interrupted ' + message
    sys.exit(2)


def build_doc(go_path, project_path, scanlogbinext):
    """Extracts metadata from the GO FITS file header, and builds
       a dictionary matching header keywords to values

    Keyword arguments:
    go_path -- string name full path to a GO FITS file
    project_path -- string name to the project_path
    scanlogbinext -- pyfits object containg the ScanLog.fits file binary header
    """
    doc = {}

    try:
        hdulist = pyfits.open(go_path)
        if len(hdulist) < 1:
            return doc
        header = hdulist[0].header
        pointer = iter(header.items())
        doc['RESTFRQ'] = ''
        doc['MSGFLAG'] = ''
    except KeyboardInterrupt:
        interrupt()
    except:
        print >> sys.stderr, 'error opening hdulist %s' % go_path
        return doc

    try:
        for i in range(0, len(header)):
            if (header.items()[i][0] != 'HISTORY'):
                doc[header.items()[i][0]] = header[i]
            if (header.items()[i][0] == 'HISTORY'):
                if '=' in header[i]:
                    name, datum = header[i].split('=', 1)
                    name = name.strip().replace('.', '_')
                    datum = ('"""' + datum
                        .strip()
                        .replace('"', '')
                        .replace("'", '')
                        + '"""')
                    try:
                        doc.update(eval('dict(config_%s=%s)' % (name, datum)))
                    except SyntaxError:
                        print >> sys.stderr, ('ignoring %s in %s' %
                                                (name, go_path))
                if (':' in header[i]):
                    if not (header[i].startswith('Configuration')):
                        if not (header[i].startswith('Catalog')):
                            name, datum = header[i].split(':', 1)
                            name = name.strip().replace('.', '_')
                            datum = ('"""' + datum
                                .strip()
                                .replace('"', '')
                                .replace("'", '')
                                + '"""')
                            try:
                                doc.update(eval('dict(catalog_%s=%s)'
                                            % (name, datum)))
                            except SyntaxError:
                                print >> sys.stderr, ('ignoring %s in %s'
                                            % (name, go_path))
    except KeyboardInterrupt:
        interrupt()
    except:
        print >> sys.stderr, 'error with config/cat section %s' % go_path
        doc['MSGFLAG'] += ' Possible aborted scan.'
        doc['BADDATA'] = 1
        return doc

    #Work on receiver, backend, chans,
    #and bandwidth from IF Fits file and ScanLog Fits file
    ifbinext = ''
    if_path = ''
    backends = []
    receiver = ''
    nchans = 0
    bandwidth = 0

    if (scanlogbinext == 0):
        doc['MSGFLAG'] += ' Scanlog.fits missing END card or does not exist.'
        doc['BADDATA'] = 1
        doc['RECEIVER'] = receiver
        doc['BACKEND'] = ', '.join(backends)
        return doc
    else:
        index = ([j for j, x in enumerate(scanlogbinext['SCAN'])
                    if x == doc['SCAN']])
        scanfiles = scanlogbinext['FILEPATH'][index]

        for sfile in scanfiles:
            if 'IF' in sfile:

                try:
                    if_path = project_path + '/IF/' + os.path.basename(sfile)
                    ifbinext = pyfits.getdata(if_path, 1)
                    receiver = ifbinext['RECEIVER'][0]
                    backendsearch = "\n".join(scanlogbinext['FILEPATH'][index])
                    backendkeys = uniqify(ifbinext['BACKEND'])
                    backends = []
                    for k in backendkeys:
                        if k in backendsearch:
                            backends.append(k)
                    if (backends == []):
                        backends = ['']
                    #Look for DCR bandwidth, if needed
                    dcrbw = ifbinext['BANDWDTH'][0]
                except:
                    print >> sys.stderr, 'IF file does not exist'
                    doc['MSGFLAG'] += ' IF file not found. '
                    doc['BADDATA'] = 1

            if (('Spectrometer' in sfile) and ('Spectrometer' in backends)):
                try:
                    spec_path = project_path + '/Spectrometer/' + \
                                    os.path.basename(sfile)
                    hduspec = pyfits.open(spec_path)
                    nchans = hduspec[0].header['NLAGS']
                    portbinext = pyfits.getdata(spec_path, 'PORT')
                    bandwidth = portbinext['BANDWDTH'][0]
                except:
                    doc['MSGFLAG'] += ' No backend recorded. '
                    doc['BADDATA'] = 1

            if (('SpectralProcessor' in sfile) and \
                ('SpectralProcessor' in backends)):
                try:
                    spec_path = project_path + '/SpectralProcessor/' + \
                        os.path.basename(sfile)
                    hdulist = pyfits.open(spec_path)
                    tdim5 = hdulist['DATA'].header['TDIM5']
                    nchans = tuple(map(int, tdim5[1:-1].split(',')))[0]
                    #RECEIVER is extension 1
                    receiverbinext = pyfits.getdata(spec_path, 'RECEIVER')
                    bandwidth = receiverbinext['BANDWD'][0]
                except:
                    doc['MSGFLAG'] += ' No backend recorded.'
                    doc['BADDATA'] = 1

            if (('DCR' in sfile) and ('DCR' in backends)):
                nchans = 1
                bandwidth = dcrbw

        try:
            doc['RECEIVER'] = receiver
            doc['BACKEND'] = ', '.join(backends)
            doc['NCHAN'] = nchans
            doc['BW'] = bandwidth
        except SyntaxError:
            print >> sys.stderr, 'ignoring %s in %s' % (name, go_path)

        return doc


def gather_project(project_path):
    """Examines each GO FITS file in an observing directory
       and generates a list of dictionaries, one dictionary per scan,
       of required GBT metadata.  Output is written to standard out.

    Keyword arguments:
    project_path -- string name to the project_path
    """
    golist = glob.glob(join(project_path, 'GO', '*'))
    golist.sort()
    try:
        #Extension 1
        scanlogbinext = pyfits.getdata(project_path +
            '/ScanLog.fits', 'ScanLog')
    except KeyboardInterrupt:
        interrupt()
    except:
        print >> sys.stderr, '  error opening scanlog %s' % \
            project_path + '/ScanLog.fits '
        scanlogbinext = 0

    filename = project_path
    filesize = getFolderSize(project_path) / 1024.0

    for go_path in golist:
        doc = build_doc(go_path, project_path, scanlogbinext)
        if (doc != {}):
            doc = derivedMetadata(doc, filename, filesize)
        doc = dict([(k, v) for k, v in doc.items() if k in outfields])
        if doc:
            writer.writerow(doc)


#-----------------------------------------------------Start main script here

project_paths = []

if len(sys.argv) == 1:
    msg = ('Sample usage - python gbtdump.py ' +
            '/export/cvlustre/vulcan/GBTDATA_NEW/* > output.csv')
    #sys.exit(msg)
    print >> sys.stderr, msg

if len(sys.argv) == 2:
    dir = sys.argv[1]
    if (dir[-1] == '/'):
        project_paths = glob.glob(dir + '/*')
        project_paths.sort()
    else:
        project_paths = [dir]

if len(sys.argv) > 2:
    project_paths = sys.argv[1:]

#-----------------------------------------------------Print out header keywords

outfields = [k for k in open('gbtindex-keywords.txt').read().split('\n') if k]
writer = csv.DictWriter(sys.stdout, outfields, \
            delimiter=',', quoting=csv.QUOTE_ALL)
headers = dict((n, n) for n in outfields)
writer.writerow(headers)
#print ','.join(outfields)

try:
    #print >> sys.stderr, 'Starting...'
    t1 = time.time()
    for project_path in project_paths:
        #print >> sys.stderr, project_path, \
        #    ': ' + str(project_paths.index(project_path) + 1) + \
        #    ' of ' + str(len(project_paths))
        gather_project(project_path)
    t2 = time.time()
    #print >> sys.stderr, t2 - t1, ' seconds'
    #print >> sys.stderr, 'Ending...'
except KeyboardInterrupt:
    interrupt()
